# Spring into Trivia

This a full attempt to create a full-stack Java web application,
complete with JWT-based authentication and external API calls to
Open Trivia Database.

## Tools used

1. Spring Boot
2. React.js
3. PostgreSQL
4. Open Trivia Database

## Goals

1. Complete backend JWT authentication with sign-up, sign-in, and logout (manually test with Postman or other API tool).
2. Wire Spring Boot backend with React front end.
3. Use front-end state management to hold authentication tokens.
4. Complete styling with Bootstrap.
5. Decouple PostgreSQL database into separate volume for production deployment.

### Credits

Open Trivia Database - <https://opentdb.com/>
