package springintotrivia.triviabackend.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import springintotrivia.triviabackend.Models.Score;

public interface ScoreRepository extends JpaRepository<Score, Long>{
	
	Score findScoresByPlayer(String player);
	
}
