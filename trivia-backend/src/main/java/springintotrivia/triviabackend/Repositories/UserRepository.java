package springintotrivia.triviabackend.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import springintotrivia.triviabackend.Models.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByUsername(String username);

}
