package springintotrivia.triviabackend.Models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "scores")
public class Score {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String player;
	private String category;
	private long score;
	
	private Score() {}
	
	public Score(long id, String player, String category, long score) {
		super();
		this.id = id;
		this.player = player;
		this.category = category;
		this.score = score;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public long getScore() {
		return score;
	}
	public void setScore(long score) {
		this.score = score;
	}
}
